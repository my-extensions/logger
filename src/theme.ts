import { FileInfo, getCallerFileInfo } from "./utils";
import { Message } from "./message";

enum ThemeType {
	default = "default",
	full = "full",
	minimal = "minimal",
}

enum LogType {
	DEFAULT = "",
	INFO = "🔵",
	WARNING = "🟠", //"🟠⚠️",
	SUCCESS = "✅", //"🟢✅",
	ERROR = "❌", //"🔴❌",
}

interface ThemeOptions {
	showIcon: boolean;
	showFilePath: boolean;
	showFileName: boolean;
	showTimer: boolean;
	showLine: boolean;
	showLineBreak: boolean;
}

class Theme {
	private theme: ThemeType;
	constructor(theme?: ThemeType) {
		this.theme = theme ? theme : ThemeType.default;
	}

	getThemedLog(type: LogType, tag?: string): string {
		// file info
		let fileInfo: FileInfo | undefined;
		if (this.needFileInfo()) {
			fileInfo = getCallerFileInfo();
		}

		// get full message
		const message = new Message(this.getOptions(), tag, fileInfo, type);
		return message.getMessage();
	}

	private showTimer(): boolean {
		return this.getOptions().showTimer;
	}

	private showFullPath(): boolean {
		return this.getOptions().showFilePath;
	}

	private showFileName(): boolean {
		return this.getOptions().showFileName;
	}

	private showLineNumber(): boolean {
		return this.getOptions().showLine;
	}

	private showIcon(): boolean {
		return this.getOptions().showIcon;
	}
	private needFileInfo(): boolean {
		const options = this.getOptions();
		return options.showFileName || options.showFilePath || options.showLine;
	}

	private getOptions(): ThemeOptions {
		switch (this.theme) {
			case ThemeType.default:
				return {
					showIcon: true,
					showFilePath: false,
					showFileName: true,
					showTimer: true,
					showLine: true,
					showLineBreak: true,
				};
			case ThemeType.full:
				return {
					showIcon: true,
					showFilePath: true,
					showFileName: true,
					showTimer: true,
					showLine: true,
					showLineBreak: true,
				};
			case ThemeType.minimal:
				return {
					showIcon: false,
					showFilePath: false,
					showFileName: false,
					showTimer: false,
					showLine: false,
					showLineBreak: false,
				};
		}
	}
}

export { Theme, ThemeType, LogType, ThemeOptions };
