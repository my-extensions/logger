import { getCallerFileInfo, FileInfo } from "./utils";
import { Theme, ThemeType, LogType } from "./theme";
import Message from "./message";

/**
 * Logger class
 */
export default class Logger {
	private theme: Theme;

	constructor(theme: ThemeType) {
		this.theme = new Theme(theme);
	}

	public d(value: any, tag?: string) {
		this.log(value, LogType.DEFAULT, tag);
	}

	public s(value: any, tag?: string) {
		this.log(value, LogType.SUCCESS, tag);
	}

	public i(value: any, tag?: string) {
		this.log(value, LogType.INFO, tag);
	}

	public w(value: any, tag?: string) {
		this.log(value, LogType.WARNING, tag);
	}

	public e(value: any, tag?: string) {
		this.log(value, LogType.ERROR, tag);
	}

	private log(value: any, type: LogType, tag?: string): void {
		const log = this.theme.getThemedLog(type, tag);
		this.call(value, type, log);

	}

	/**
	 *
	 * @param type
	 * @param value
	 */
	private call(value: any, type: LogType, log: string) {
		switch (type) {
			case LogType.INFO:
				console.info(log, value);
				break;
			case LogType.WARNING:
				console.warn(log, value);
				break;
			case LogType.SUCCESS:
				console.log(log, value);
				break;
			case LogType.ERROR:
				console.error(log, value);
				break;
			default:
				console.log(log, value);
				break;
		}
	}
}
