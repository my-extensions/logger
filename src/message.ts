import { FileInfo } from "./utils";
import { LogType, ThemeOptions } from "./theme";

interface ThemeData {
	value: string;
	tag?: string;
	icon?: string;
}

export default class Message {
	private themeOptions: ThemeOptions;
	private tag?: string;
	private fileInfo?: FileInfo;
	private icon?: LogType;

	constructor(
		themeOptions: ThemeOptions,
		tag?: string,
		fileInfo?: FileInfo,
		icon?: LogType
	) {
		this.themeOptions = themeOptions;
		this.tag = tag;
		this.fileInfo = fileInfo;
		this.icon = icon;
	}

	getMessage(): string {
		return this.setMessage();
	}

	private setMessage(): string {
		const icon = this.setIcon();
		const tag = this.setTag();
		const fileInfo = this.setFileInfo();
		const lineBreak = this.setLineBreak();
		return icon + tag + fileInfo + lineBreak;
	}

	private setIcon(): string {
		return this.icon ? `${this.icon} ~ ` : "";
	}

	private setTag(): string {
		return this.tag ? `${this.tag.toUpperCase()} ~ ` : "";
	}

	private setFileInfo(): string {
		if (!this.fileInfo) return "";

		let fileInfoResult: string = "";
		// full path
		if (this.themeOptions.showFilePath) {
			const fullPath = this.fileInfo.fullPath;
			fileInfoResult += `${
				this.isEmptyOrNilString(fullPath) ? "" : `${fullPath} ~ `
			}`;
		}

		// file name
		if (this.themeOptions.showFileName) {
			const name = this.fileInfo.name;
			fileInfoResult += `${this.isEmptyOrNilString(name) ? "" : `${name} ~ `}`;
		}

		// line number
		if (this.themeOptions.showLine) {
			const lineNumber = this.fileInfo.lineNumber;
			fileInfoResult += `${
				lineNumber === undefined ? "" : `line ${lineNumber} ~ `
			}`;
		}

		// timer
		if (this.themeOptions.showTimer) {
			fileInfoResult += `${this.getCurrentTime()} ~ `;
		}

		return fileInfoResult.slice(0, -2); // remove separator
	}

	private setLineBreak(): string {
		return this.themeOptions.showLineBreak ? "\n" : "";
	}

	private isEmptyOrNilString(str?: string): boolean {
		if (!str) return false;
		return str.length === 0;
	}

	private getCurrentTime(): string {
		var now = new Date();
		var time = `${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}:${now.getMilliseconds()}`;
		return time.toString();
	}
}

export { Message };
