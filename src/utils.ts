import * as path from "path";

/**
 * File info description
 *
 * @interface FileInfo
 * @member {string} fullPath
 * @member {string} name
 * @member {string} lineNumber
 */
interface FileInfo {
	fullPath?: string;
	name?: string;
	lineNumber?: number;
}

/**
 * Get caller file infos
 * @returns {FileInfo}
 */
function getCallerFileInfo(): FileInfo {
	const err = new Error();

	Error.prepareStackTrace = (_, stack) => stack;

	const stack = err.stack as any;

	Error.prepareStackTrace = undefined;

	const fileStack = stack[1];

	return {
		fullPath: fileStack.getFileName(),
		name: _getFileNameFromPath( fileStack.getFileName()),
		lineNumber: fileStack.getLineNumber(),
	};
}

/**
 * Get file name from file full path
 * @param filePath full path of a file
 * @returns {string} filename
 */
const _getFileNameFromPath = (filePath: string): string => {
	return path.basename(filePath);
};

export { getCallerFileInfo, FileInfo };
